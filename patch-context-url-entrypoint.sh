#!/usr/bin/env sh
# vim: set ts=2 sts=2 sw=2 et number:

# normalize context url and delimit without a trailing slash
contextURL=$(echo "${CONTEXT_URL}" | sed -E 's@(://)|/$|((/)/+)@\1\3@g')

# find files that contain dummy url
pageFiles=$(grep -lr "${DUMMY_URL}" /usr/share/nginx/html)

# loop all files with the context url and patch them with the real context url
for f in ${pageFiles}; do
  sed -i -E "s@${DUMMY_URL}/?@${contextURL}/@g" "$f"
done

# find files that contain relative URLs
relFiles=$(grep -rlE ' (src|href)="?/' /usr/share/nginx/html/)

# loop all files with only relative URLs and patch them with the real context url
for f in ${relFiles}; do
  sed -i -E 's@((src|href)="?)/?@\1'"${contextURL}"'/@g' "$f"
done